//RF2.4
#include <RHReliableDatagram.h>
#include <RH_NRF24.h>
#include <SPI.h>
#define CLIENT_ADDRESS 1
#define SERVER_ADDRESS 2
RH_NRF24 driver(8, 7);   // SPI bus 8:CE 7:CSN
RHReliableDatagram manager(driver, CLIENT_ADDRESS);
int rockerCommand[6];
float height = 10.0;
float dir = 10.0;
String data = "";
String firstPartData = "";
String secondPartData = "";
String command = "";

#include <Wire.h>
#include <SFE_BMP180.h>
SFE_BMP180 pressure;
double flightHight;

#include <HMC5883L.h>
HMC5883L compass;
int heading;

void setup() {
  Serial.begin(57600);
  // RF2.4
  if (!manager.init())
    Serial.println("NFR24 ini failed");
//  acknowledgePID();
  setUpBmp180();
  setUpHMC5883L();
}

void loop() {
  flightHight =  getAltitude();
  heading = getHeadingDegree();
  String data = "0:"+(String)flightHight+",1:"+(String)heading+",";
  data.trim();
  command = radio(data);
  addCommandElementId(command);
  writeDataBySerial(command);
}





