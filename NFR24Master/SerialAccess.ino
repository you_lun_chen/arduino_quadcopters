//String readDataFromSerial() {
//  String deviceData = "";
//  deviceData.reserve(250);
//  int count = 0;
//  while (Serial.available()) {
//    // get the new byte:
//    char inChar = (char)Serial.read();
//    // add it to the inputString:
//    deviceData += inChar;
//    if (inChar == ',') {
//      count++;
//      if (count == 4) {
//        break;
//      }
//    }
//  }
//  deviceData.trim();
//  return deviceData;
//}

void writeDataBySerial(String command) {
  if (command.length() > 0) {
    command.trim();
    char* buf = (char*) malloc(sizeof(char) * command.length() + 1);
    command.toCharArray(buf, command.length() + 1);
    Serial.write(buf);
    Serial.flush();
    free(buf);
  }
}

//void acknowledgePID() {
//  writeDataBySerial("Master get ready!");
//}

void addCommandElementId(String commandString) {
  int index = 0;
  String newCommandWithId = "";
  while (commandString.length() > 0) {
    int split = commandString.indexOf(",");
    newCommandWithId += String(index) + ":" + commandString.substring(0, split) + ",";
    commandString.remove(0, commandString.substring(0, split).length() + 1);
    index++;
  }
  command = newCommandWithId;
}

