char pressureStatus, temperatureStatus;
double temperature, basePressure, absolutePressure, altitude;
#define ALTITUDE 1655.0 // don't change it

void setUpBmp180() {
  if (pressure.begin()) {
    Serial.println("BMP180 init success");
    getTemperature();
    getPressure(basePressure);
  } else {
    // Oops, something went wrong, this is usually a connection problem,
    // see the comments at the top of this sketch for the proper connections.

    Serial.println("BMP180 init fail\n\n");
    while (1); // Pause forever.
  }
}

double getAltitude() {
  getPressure(absolutePressure);
  if (absolutePressure > 0 && basePressure > 0) {
    return pressure.altitude(absolutePressure, basePressure);
  } else {
    return -1.0;
  }
}

void getPressure(double &pressureValue) {
  getTemperature();
  pressureStatus = pressure.startPressure(3);
  if (pressureStatus != 0 && temperatureStatus != 0)
  {
    // Wait for the measurement to complete:
    delay(pressureStatus);

    // Retrieve the completed pressure measurement:
    // Note that the measurement is stored in the variable P.
    // Note also that the function requires the previous temperature measurement (T).
    // (If temperature is stable, you can do one temperature measurement for a number of pressure measurements.)
    // Function returns 1 if successful, 0 if failure.

    pressureStatus = pressure.getPressure(pressureValue, temperature);
    if (pressureStatus == 0)
    {
      pressureValue = -1;
    }
  }
}

void getTemperature() {
  temperatureStatus = pressure.startTemperature();
  if (temperatureStatus != 0)
  {
    // Wait for the measurement to complete:
    delay(temperatureStatus);

    // Retrieve the completed temperature measurement:
    // Note that the measurement is stored in the variable T.
    // Function returns 1 if successful, 0 if failure.

    temperatureStatus = pressure.getTemperature(temperature);
    if (temperatureStatus == 0)
    {
      temperature = -1;
    }
  }
}
