String radio(String deviceData) {
  uint8_t data[sizeof(deviceData) * 4];
  deviceData.getBytes(data, sizeof(deviceData) * 4);
  if (manager.sendtoWait(data, sizeof(data), SERVER_ADDRESS)) {
    return receiveDataFromRadio();
  }
  return "no data";
}

String receiveDataFromRadio() {
  uint8_t buf[120];
  // Now wait for a reply from the server
  uint8_t len = sizeof(buf);
  uint8_t from;
  if (manager.recvfromAckTimeout(buf, &len, 2000, &from))
  {
    return String((char*)buf);
  }
  return "no data";
}

void cutDataIntoPart(String data) {
  data = removeDuplitcateDataIndex(data);
  int split = data.indexOf("/");
  firstPartData = data.substring(0, split);
  data.remove(0, firstPartData.length() + 1);
  secondPartData = data;
}

String removeDuplitcateDataIndex(String data) {
  if (data.indexOf("0:") != data.lastIndexOf("0:")) {
    data.remove(0, data.lastIndexOf("0:"));
  }
  return data;
}
