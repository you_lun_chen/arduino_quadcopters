#define INTERRUPT_PIN 2  // use pin 2 on Arduino Uno & most boards
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer
Quaternion q;           // [w, x, y, z]         quaternion container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
VectorFloat gravity;    // [x, y, z]            gravity vector

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
  mpuInterrupt = true;
}

void setUpMPU6050() {
  while (!Serial); // wait for Leonardo enumeration, others continue immediately
  mpu.initialize();
  pinMode(INTERRUPT_PIN, INPUT);
  // verify connection
  //  Serial.println(F("Testing device connections..."));
  //  Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
  //  Serial.println(F("Initializing DMP..."));
  devStatus = mpu.dmpInitialize();
  // supply your own gyro offsets here, scaled for min sensitivity
  mpu.setXGyroOffset(82);
  mpu.setYGyroOffset(-14);
  mpu.setZGyroOffset(-18);
  mpu.setZAccelOffset(1693); // 1688 factory default for my test chip
  // make sure it worked (returns 0 if so)
  if (devStatus == 0) {
    // turn on the DMP, now that it's ready
    //    Serial.println(F("Enabling DMP..."));
    mpu.setDMPEnabled(true);

    // enable Arduino interrupt detection
    //    Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    //    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;

    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();
  } else {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    //    Serial.print(F("DMP Initialization failed (code "));
    //    Serial.print(devStatus);
    //    Serial.println(F(")"));
  }
}

void getBalanceData() {
  // if programming failed, don't try to do anything
  if (!dmpReady) return;

  // wait for MPU interrupt or extra packet(s) available
  while (!mpuInterrupt && fifoCount < packetSize) {
    // other program behavior stuff here
    // .
    // .
    // .
    // if you are really paranoid you can frequently test in between other
    // stuff to see if mpuInterrupt is true, and if so, "break;" from the
    // while() loop to immediately process the MPU data
    // .
    // .
    // .
  }

  // reset interrupt flag and get INT_STATUS byte
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

  // get current FIFO count
  fifoCount = mpu.getFIFOCount();

  // check for overflow (this should never happen unless our code is too inefficient)
  if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
    // reset so we can continue cleanly
    mpu.resetFIFO();
    //    Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
  } else if (mpuIntStatus & 0x02) {
    // wait for correct available data length, should be a VERY short wait
    while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

    // read a packet from FIFO
    mpu.getFIFOBytes(fifoBuffer, packetSize);
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
    //        Serial.print("ypr\t");
    //        Serial.print(ypr[0] * 180 / M_PI);
    //        Serial.print("\t");
    //        Serial.print(ypr[1] * 180 / M_PI);
    //        Serial.print("\t");
    //        Serial.println(ypr[2] * 180 / M_PI);
  }
  composate(ypr[1] * 180 / M_PI, ypr[2] * 180 / M_PI, ypr[0] * 180 / M_PI);
  mpu.resetFIFO();
}

void parseCommand(String commandString, int command[]) {
  while (commandString.length() > 0) {
    int split = commandString.indexOf(",");
    String valueElement = commandString.substring(0, split);
    int elementIndex = getIndex(valueElement);
    String value = getValue(valueElement);
    command[elementIndex] = value.toInt();
    commandString.remove(0, commandString.substring(0, split).length() + 1);
  }
}

//馬達定位
//前方
//moter2    moter1
//moter4    moter3
void composate(float pich, float roll, float yaw) {
  motorComposate[0] =  - pich - roll - yaw;//motor 1
  motorComposate[1] =  - pich + roll + yaw;//motor 2
  motorComposate[2] =  + pich - roll + yaw;//motor 3
  motorComposate[3] =  + pich + roll - yaw;//motor 4
}

void getComposateValueWhenBalance() {
  int totalSampleTime = 4;
  for (int sampleTimes = 0; sampleTimes < totalSampleTime; sampleTimes++) {
    getBalanceData();
    saveComposateValueWhenBalance();
  }
  for (int motorComposateValueWhenBalanceIndex = 0; motorComposateValueWhenBalanceIndex < 4; motorComposateValueWhenBalanceIndex++) {
    motorComposateValueWhenBalance[motorComposateValueWhenBalanceIndex] = motorComposateValueWhenBalance[motorComposateValueWhenBalanceIndex]/totalSampleTime;
  }
}

void saveComposateValueWhenBalance() {
  for (int i = 0; i < 4; i++) {
    motorComposateValueWhenBalance[i] = motorComposateValueWhenBalance[i] + motorComposate[i];
  }
}

void calculateBalanceOffset(int arrayindex) {
  float motorComposateValueAfterRemoveBalanceOffset = motorComposate[arrayindex] - motorComposateValueWhenBalance[arrayindex];
  if (motorComposateValueAfterRemoveBalanceOffset < 0) {
    motorComposateValueAfterRemoveBalanceOffset = 0;
  }
  motorComposate[arrayindex] = motorComposateValueAfterRemoveBalanceOffset;
}

String getValue(String element) {
  int split = element.indexOf(":");
  String value = element.substring(split + 1, element.length());
  return value;
}

int getIndex(String element) {
  int split = element.indexOf(":");
  String indexString = element.substring(0, split);
  return indexString.toInt();
}
