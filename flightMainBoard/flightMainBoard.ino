#include <Wire.h>
#include "MPU6050_6Axis_MotionApps20.h"

MPU6050 mpu;
int rockerCommand[6];
float motorComposate[4];
float motorComposateValueWhenBalance[4];
String command;
void setup()
{
  // join I2C bus (I2Cdev library doesn't do this automatically)
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  Wire.begin();
  Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  Fastwire::setup(400, true);
#endif
  Serial.begin(57600);
  setUpMPU6050();
  getComposateValueWhenBalance();
  setUpMotors();
}
void loop() {
  getBalanceData();
  for (int motorComposateIndex= 0; motorComposateIndex < 4; motorComposateIndex++) {
    calculateBalanceOffset(motorComposateIndex);
  }
  controlMotor();
}

void serialEvent() {
  command = readDataFromSerial();
  parseCommand(command, rockerCommand);
  controlMotor();
}
