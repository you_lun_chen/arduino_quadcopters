int motorNo1 = 11;
int motorNo2 = 3;
int motorNo3 = 9;
int motorNo4 = 10;

void changeMotorSpeed(int motor, long speedModification) { //analogWrite values from 0 to 255
  analogWrite(motor, limitMotorSpeed(speedModification));
}

void setUpMotors() {
  pinMode(motorNo1, OUTPUT);
  pinMode(motorNo2, OUTPUT);
  pinMode(motorNo3, OUTPUT);
  pinMode(motorNo4, OUTPUT);
  initializeMotor(motorNo1);
  initializeMotor(motorNo2);
  initializeMotor(motorNo3);
  initializeMotor(motorNo4);
}

void initializeMotor(int motor) {
  changeMotorSpeed(motor, 255);
  delay(30);
  changeMotorSpeed(motor, 125);
  delay(30);
  changeMotorSpeed(motor, 50);
  delay(30);
  changeMotorSpeed(motor, 0);
  delay(30);
}

//馬達定位
//前方
//moter2    moter1
//moter4    moter3
long baseSpeedRate = 30;

void checkMotorBaseSpeedRate() {
  if (baseSpeedRate - rockerCommand[0] < 15 && baseSpeedRate - rockerCommand[0] > -15) {
    baseSpeedRate = rockerCommand[0];
  }
}

void controlMotor() {
  checkMotorBaseSpeedRate();
  //  Serial.println("checkMotorBaseSpeedRate");
  //  Serial.println(baseSpeedRate);
  long clockWiseOffset = rockerCommand[1];
  long forwardAndBackWard = rockerCommand[3];
  long leftAndRight = rockerCommand[4];

//  Serial.println("clockWiseOffset");
//  Serial.println(clockWiseOffset);
  long speedForMotor1 = baseSpeedRate + clockWiseOffset + motorComposate[0];
  long speedForMotor2 = baseSpeedRate - clockWiseOffset + motorComposate[1];
  long speedForMotor3 = baseSpeedRate - clockWiseOffset + motorComposate[2];
  long speedForMotor4 = baseSpeedRate + clockWiseOffset + motorComposate[3];
//   Serial.println("speedForMotor1");
//  Serial.println(speedForMotor1);
//   Serial.println("speedForMotor2");
//  Serial.println(speedForMotor2);
//   Serial.println("speedForMotor3");
//  Serial.println(speedForMotor3);
//   Serial.println("speedForMotor4");
//  Serial.println(speedForMotor4);
  if (forwardAndBackWard > 0) {
    //forward
    speedForMotor3 = speedForMotor3 + forwardAndBackWard;
    speedForMotor4 = speedForMotor4 + forwardAndBackWard;
  } else if (forwardAndBackWard < 0){
    //backward
    speedForMotor1 = speedForMotor1 - forwardAndBackWard;
    speedForMotor2 = speedForMotor2 - forwardAndBackWard;
  }

  if (leftAndRight > 0) {
    //turn right
    speedForMotor4 = speedForMotor4 + leftAndRight;
    speedForMotor2 = speedForMotor2 + leftAndRight;
  } else if(leftAndRight < 0) {
    //turn left
    speedForMotor1 = speedForMotor1 - leftAndRight;
    speedForMotor3 = speedForMotor3 - leftAndRight;
  }

  changeMotorSpeed(motorNo1, limitMotorSpeed(speedForMotor1));
  changeMotorSpeed(motorNo2, limitMotorSpeed(speedForMotor2));
  changeMotorSpeed(motorNo3, limitMotorSpeed(speedForMotor3));
  changeMotorSpeed(motorNo4, limitMotorSpeed(speedForMotor4));
  
}

int limitMotorSpeed(int motorSpeed) {
  if (motorSpeed > 255) {
    return 255;
  } else if (motorSpeed < 0) {
    return 0;
  } else {
    return motorSpeed;
  }
}

void landing() {
  rockerCommand[0] = 10;
  rockerCommand[1] = 10;
  rockerCommand[2] = 10;
  rockerCommand[3] = 10;
  motorComposate[0] = 0;
  motorComposate[1] = 0;
  motorComposate[2] = 0;
  motorComposate[3] = 0;
}

