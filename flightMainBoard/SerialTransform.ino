String readDataFromSerial() {
  String deviceData = "";
  deviceData.reserve(200);
  int count = 0;
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    deviceData += inChar;
    if (inChar == ',') {
      count++;
      if (count == 6) {
        break;
      }
    }
  }
  deviceData.trim();
  return deviceData;
}


