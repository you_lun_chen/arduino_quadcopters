int motorNo1 = 6;
int motorNo2 = 3;
int motorNo3 = 9;
int motorNo4 = 5;

void setup() {
  Serial.begin(9600);
  pinMode(motorNo1, OUTPUT);
  pinMode(motorNo2, OUTPUT);
  pinMode(motorNo3, OUTPUT);
  pinMode(motorNo4, OUTPUT);
  initializeMotor(motorNo1);
  initializeMotor(motorNo2);
  initializeMotor(motorNo3);
  initializeMotor(motorNo4);

  changeMotorSpeed(motorNo1, 135);
  changeMotorSpeed(motorNo2, 135);
  changeMotorSpeed(motorNo3, 135);
  changeMotorSpeed(motorNo4, 200);
}

void initializeMotor(int motor) {
  changeMotorSpeed(motor, 255);
  delay(20);
  changeMotorSpeed(motor, 125);
  delay(20);
  changeMotorSpeed(motor, 50);
  delay(20);
  changeMotorSpeed(motor, 0);
}

void loop() {
 
}

void changeMotorSpeed(int motor, long speedModification) { //analogWrite values from 0 to 255
  analogWrite(motor, speedModification);
}

