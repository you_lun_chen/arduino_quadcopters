int speedPin = 11;   
int analogReadPin = A3;   
int val = 0;         
void setup() {
  Serial.begin(9600);
  TCCR0B = TCCR0B & B11111000 | B00000100;    // set timer 0 divisor to 256 for PWM frequency of 244.14 Hz this will help for D5 D6
  pinMode(speedPin, OUTPUT);   // sets the pin as output
}
void loop() {
  val = analogRead(analogReadPin);   // read the input pin
  val = val / 4;
  if (val > 255)
  {
    val = 255;
  }
  if (val < 0) {
    val = 0;
  }
  analogWrite(speedPin, val); // analogRead values go from 0 to 1023, analogWrite values from 0 to 255
  Serial.println(val);
}


