

/* =========================================================================
   NOTE: In addition to connection 3.3v, GND, SDA, and SCL, this sketch
   depends on the MPU-6050's INT pin being connected to the Arduino's
   external interrupt #0 pin. On the Arduino Uno and Mega 2560, this is
   digital I/O pin 2.
 * ========================================================================= */

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

float balanceYaw = 0.0;
float balancePich = 0.0;
float balanceRoll = 0.0;

float composateValue = 0.5;

// orientation/motion vars

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high

void dmpDataReady() {
  mpuInterrupt = true;
}

void setUpMUPDevice(MPU6050 &mpu) {
  mpu.initialize();

  // verify connection
//   Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
  // load and configure the DMP
  devStatus = mpu.dmpInitialize();

  // supply your own gyro offsets here, scaled for min sensitivity
  initializeOffset(mpu);

  // make sure it worked (returns 0 if so)
  if (devStatus == 0) {
    // turn on the DMP, now that it's ready
    mpu.setDMPEnabled(true);

    // enable Arduino interrupt detection
    attachInterrupt(0, dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
//    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;

    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();
  } else {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
  }
}

void checkMPUState(MPU6050 &mpu) {
  // if programming failed, don't try to do anything
  if (!dmpReady) return;
  // wait for MPU interrupt or extra packet(s) available
  while (!mpuInterrupt) {
//    Serial.println("first while");
    // if you are really paranoid you can frequently test in between other
    // stuff to see if mpuInterrupt is true, and if so, "break;" from the
    // while() loop to immediately process the MPU data
  }
  //  Serial.println("reset interrupt flag and get INT_STATUS byte");
  // reset interrupt flag and get INT_STATUS byte
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

  // get current FIFO count
  fifoCount = mpu.getFIFOCount();
  // check for overflow (this should never happen unless our code is too inefficient)
  if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
    // reset so we can continue cleanly
    mpu.resetFIFO();
    Serial.println(F("FIFO overflow!"));
    // otherwise, check for DMP data ready interrupt (this should happen frequently)
  } else if (mpuIntStatus & 0x02) {
    fifoCount = mpu.getFIFOCount();
    //     Serial.println("read a packet from FIFO");
    // read a packet from FIFO
    mpu.getFIFOBytes(fifoBuffer, packetSize);
    mpu.resetFIFO();
    // track FIFO count here in case there is > 1 packet available
    // (this lets us immediately read more without waiting for an interrupt)
    fifoCount -= packetSize;
    takeBalance(mpu);
  }
}

void initializeOffset(MPU6050 &mpu) {
  mpu.setXGyroOffset(95);
  mpu.setYGyroOffset(-14);
  mpu.setZGyroOffset(-7);
  mpu.setXAccelOffset(-2812); 
  mpu.setYAccelOffset(-3527); 
  mpu.setZAccelOffset(1771); 
}

void takeBalance(MPU6050 &mpu) {
  //   Serial.println("takeBalance");
  mpu.dmpGetQuaternion(&q, fifoBuffer);
  mpu.dmpGetGravity(&gravity, &q);
  mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
  float yaw = ypr[0] * 180 / M_PI;//yaw
  float pich = ypr[1] * 180 / M_PI;//pich
  float roll = ypr[2] * 180 / M_PI;//roll
//  Serial.println("pich");
//  Serial.println(pich);
//  Serial.println("roll");
//  Serial.println(roll);
  float deltaYaw = yaw - balanceYaw;
  float deltaPich = pich - balancePich;
  float deltaRoll = roll - balanceRoll;
  //  Serial.println("deltaPich");
  //  Serial.println(deltaPich);
  //  Serial.println("deltaRoll");
  //  Serial.println(deltaRoll);
  if (!isBalance(deltaPich) || !isBalance(deltaRoll)) {
    composate(deltaPich, deltaRoll);
  }
}

//馬達定位
//前方
//moter2    moter1
//moter4    moter3
void composate(float pich, float roll) {
  float composateForRoll = (roll / 18) * composateValue;
  float composateForPich = (pich / 18) * composateValue;
  //  Serial.println(composateForPich);
  motorComposate[0] = -composateForRoll;//motor 1
  motorComposate[1] = composateForRoll;//motor 2
  motorComposate[2] = -composateForRoll;//motor 3
  motorComposate[3] = composateForRoll;//motor 4


  motorComposate[0] = motorComposate[0] - composateForPich;
  motorComposate[1] = motorComposate[1] - composateForPich;
  motorComposate[2] = motorComposate[2] + composateForPich;
  motorComposate[3] = motorComposate[3] + composateForPich;
}


bool isBalance(float oritand) {
  if (oritand < 8 && oritand > -8) {
    motorComposate[0] = 0;
    motorComposate[1] = 0;
    motorComposate[2] = 0;
    motorComposate[3] = 0;
    return true;
  } else {
    return false;
  }
}

