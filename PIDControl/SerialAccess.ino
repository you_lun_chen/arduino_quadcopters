String readDataFromSerial() {
  String deviceData = "";
  deviceData.reserve(200);
  int count = 0;
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    deviceData += inChar;
    if (inChar == ',') {
      count++;
      if (count == 6) {
        break;
      }
    }
    if (inChar == '!') {
      break;
    }
  }
  deviceData.trim();
  return deviceData;
}

void writeDataBySerial(String command) {
  if (command.length() > 0) {
    command.trim();
    char* buf = (char*) malloc(sizeof(char) * command.length() + 1);
    command.toCharArray(buf, command.length() + 1);
    Serial.write(buf);
    Serial.flush();
    free(buf);
  }
}

void parseCommand(String commandString, int command[]) {
  while (commandString.length() > 0) {
    int split = commandString.indexOf(",");
    String valueElement = commandString.substring(0, split);
    int elementIndex = getIndex(valueElement);
    String value = getValue(valueElement);
    command[elementIndex] = value.toInt();
    commandString.remove(0, commandString.substring(0, split).length() + 1);
  }
}

String getValue(String element) {
  int split = element.indexOf(":");
  String value = element.substring(split + 1, element.length());
  return value;
}

int getIndex(String element) {
  int split = element.indexOf(":");
  String indexString = element.substring(0, split);
  return indexString.toInt();
}
