String collectDeviceData() {
  float height = measureAltitude(pressure);
  float dir = tellDirection(Compass);
  int batteryVal = analogRead(detectBatteryPin);
  String data = "0:"+String(height) + ",1:" + String(dir) + ",/2:" + String(batteryVal) + ",3:" + String(baseSpeedRate) + ",";
  data.trim();
  return  data;
}

