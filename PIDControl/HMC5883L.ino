void setUpCompassDevice(HMC5883L_Simple &Compass) {
  Compass.SetDeclination(-4, 33, 'W');
  Compass.SetSamplingMode(COMPASS_SINGLE);
  Compass.SetScale(COMPASS_SCALE_400);
  Compass.SetOrientation(COMPASS_HORIZONTAL_X_NORTH);
}

float tellDirection(HMC5883L_Simple &Compass) {
  float heading = Compass.GetHeadingDegrees();
  return heading;
}
