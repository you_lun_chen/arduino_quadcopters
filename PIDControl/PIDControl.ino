#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"
#include "HMC5883L_Simple.h"
#include "SFE_BMP180.h"


// class default I2C ad9dress is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;
//MPU6050 mpu(0x69); // <-- use for AD0 high

HMC5883L_Simple Compass;

SFE_BMP180 pressure;
double baseline;


//Rocker command
float motorComposate[4];
String rockerCommandString;
bool rockerCommandRead = false;
int rockerCommand[6];
String returnVal = "";
int detectBatteryPin = A7;
void setup() {
  //TCCR0B = TCCR0B & B11111000 | B00000100;    // set timer 0 divisor to 256 for PWM frequency of 244.14 Hz this will help for D5 D6
  Wire.begin();
  TWBR = 12; // 400kHz I2C clock (200kHz if CPU is 8MHz)
  Serial.begin(57600);
  setUpMotors();
  setUpMUPDevice(mpu);
  setUpCompassDevice(Compass);
  setUpTemperAndHeightDevice(pressure);
  baseline = getPressure(pressure);
}

void loop() {
  checkMPUState(mpu);
  returnVal = collectDeviceData();
  controlMotor();
  writeDataBySerial(returnVal);
}

void serialEvent() {
  String command = readDataFromSerial();
  if (command == "Master get ready!") {
    writeDataBySerial("PID get ready!");
  } else if (command == "Let's Start!") {
    writeDataBySerial("0,0,0,0,");
  } else {
    parseCommand(command, rockerCommand);
  }
}






