int battryEmptyLine = 810;
bool isClickRightRocker = false;
void checkBatteryStorage() {
  if (returnValueFromFlight[2].toInt() <= battryEmptyLine) {
    hasClickedRightRocker();
    if (!isClickRightRocker) {
      analogWrite(buzzeyPin, 125);
    } else {
      analogWrite(buzzeyPin, 0);
    }
  } else {
    analogWrite(buzzeyPin, 0);
  }
}

void hasClickedRightRocker() {
  int buttonState = digitalRead(rightRockerClick);
  if (buttonState == LOW) {
    isClickRightRocker = true;
  }
}

