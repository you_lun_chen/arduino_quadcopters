//arduino 1.6.6版本IDE  IIC 1602LCD 液晶 显示不正常 只显示第一个首字符！
//
//自从1.6.6版本的IDE出来后，不断有反应LCD 1602 IIC液晶显示不正常，打印一字符串，却只显示第一个字符！最初解决办法换低版本IDE，就可以解决该问题！
//现在问题找出来了，只需要更改 LiquidCrystal_I2C.cpp  文件中的一个语句就可以了，更改如下：
//
//inline size_t LiquidCrystal_I2C::write(uint8_t value) {
//        send(value, Rs);
//        return 0; 改为 return 1;
//}


void setUpLiquidCrystal(LiquidCrystal_I2C &lcd) {
  lcd.init();                      // initialize the lcd
  // Print a message to the LCD.
  lcd.backlight();
}

void displayInformation() {
  String hei = "height:" + returnValueFromFlight[0];
//  Serial.println("height");
//  Serial.println(hei);
  String dir = "dir:" + returnValueFromFlight[1];
  String motorSpeed = "Speed:" + (String)rockerRegist[0];
  String batteryMessage;
  if (returnValueFromFlight[2].toInt() <= battryEmptyLine) {
    batteryMessage = "run out of battery";
  } else {
    batteryMessage = "battery:" + returnValueFromFlight[2];
  }
  delay(100);
  // 清除舊訊息
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(hei);
  lcd.setCursor(0, 1);
  lcd.print(dir);
  lcd.setCursor(0, 2);
  lcd.print(motorSpeed);
  lcd.setCursor(0, 3);
  lcd.print(batteryMessage);
}
