int rockerVauleChangeSpeed = 500;
void getRockerValue() {
  if (isValid(leftRockerX)) {
    rockerRegist[0] = rockerRegist[0] + (analogRead(leftRockerX) - 500) / rockerVauleChangeSpeed;
    limitRockerRegist(0);
  }
  if (isValid(leftRockerY)) {
    rockerRegist[1] = (analogRead(leftRockerY) - 500) / rockerVauleChangeSpeed;
  }
  rockerRegist[2] = digitalRead(leftRockerClick);
  if (isValid(rightRockerX)) {
    rockerRegist[3] = (analogRead(rightRockerX) - 500) / rockerVauleChangeSpeed;
  }
  if (isValid(rightRockerY)) {
    rockerRegist[4] = (analogRead(rightRockerY) - 500) / rockerVauleChangeSpeed;
  }
  rockerRegist[5] = digitalRead(rightRockerClick);
}

bool isValid(int input) {
  if (input > 505 || input < 495) {
    return true;
  }
  return false;
}

void limitRockerRegist(int index) {
  if (rockerRegist[index] > 255) {
    rockerRegist[index] = 255;
  } else if (rockerRegist[index] < 0) {
    rockerRegist[index] = 0;
  }
}


