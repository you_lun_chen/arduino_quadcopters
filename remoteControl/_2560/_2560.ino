#include <RHReliableDatagram.h>
#include <RH_NRF24.h>
#include <SPI.h>
//LiquidCrystal display
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display

#define CLIENT_ADDRESS 1
#define SERVER_ADDRESS 2

RH_NRF24 driver(3, 2);   // SPI bus CE:3 CSN:2
// Class to manage message delivery and receipt, using the driver declared above
RHReliableDatagram manager(driver, SERVER_ADDRESS);
//rocker control
int leftRockerX = A0; //control up, down:values[0]
int leftRockerY = A1; //control clock wise, anti clock wise:values[1]
const int leftRockerClick = 48; //start device and statr to fly:values[2]
int rightRockerX = A2; //control right, left:values[3]
int rightRockerY = A3; //control forward, backward: values[4]
const int rightRockerClick = 49;//start device and statr to fly: values[5]
int buzzeyPin = 45;


int rockerRegist[6]  = {0, 0, 1, 0, 0, 1};
String returnValueFromFlight[4] = {"0","0","0","0"};
bool firstTimeInitialize;
void setup() {
  Serial.begin(57600);
  Wire.begin();
  pinMode(leftRockerClick, INPUT);
  pinMode(rightRockerClick, INPUT);
  pinMode(buzzeyPin, OUTPUT);
  setUpConnection(manager);
  setUpLiquidCrystal(lcd);
  firstTimeInitialize = true;
}

void loop()
{
    getRockerValue();
    sendCommandToFlight();
    checkBatteryStorage();
    displayInformation();
    syncWithFlightSpeed();
}


