void setUpConnection(RHReliableDatagram &manager) {
  if (!manager.init())
    Serial.println("init failed");
  // Defaults after init are 2.402 GHz (channel 2), 2Mbps, 0dBm
}

String converToFullCommand(int values[]) {
  String dataInString;
  for (int index = 0; index < 6 ; index++) {
    dataInString = dataInString + values[index] + ",";
  }
  return dataInString;
}

void sendCommandToFlight() {
  String command = converToFullCommand(rockerRegist);
  //Serial.println("command:"+command);
  uint8_t data[sizeof(rockerRegist) * 2];
  command.getBytes(data, sizeof(rockerRegist) * 2);
  // Dont put this on the stack:
  uint8_t buf[255];
  Serial.println("Sending to nrf24_reliable_datagram_server");

  if (manager.available())
  {
    uint8_t len = sizeof(buf);
    uint8_t from;
    if (manager.recvfromAck(buf, &len, &from))
    {
      String returnVal = String((char*)buf);
//      Serial.println("returnVal");
//      Serial.println(returnVal);
      parseValue(returnVal, returnValueFromFlight);
      if (firstTimeInitialize) {
        rockerRegist[0] =  30;
        firstTimeInitialize = false;
      }
      // Send a reply back to the originator client
      if (!manager.sendtoWait(data, sizeof(data), from))
        Serial.println("sendtoWait failed");
    }
  }
}

void parseValue(String valueString, String values[]) {
  while (valueString.length() > 0) {
    int split = valueString.indexOf(",");
    String valueElement = valueString.substring(0, split);
    int elementIndex = getIndex(valueElement);
    String value = getValue(valueElement);
    values[elementIndex] = value;
    valueString.remove(0, valueString.substring(0, split).length() + 1);
  }
}

String getValue(String element) {
  int split = element.indexOf(":");
  String value = element.substring(split + 1, element.length());
  return value;
}

int getIndex(String element) {
  int split = element.indexOf(":");
  String indexString = element.substring(0, split);
  return indexString.toInt();
}

void syncWithFlightSpeed() {
  int buttonState = digitalRead(leftRockerClick);
  if (buttonState == LOW) {
    rockerRegist[0] =  returnValueFromFlight[3].toInt();
  }
}

